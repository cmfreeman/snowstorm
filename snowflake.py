# -*- coding: utf-8 -*-

import pickle
import os

class sentence():
    def __init__(self,sentence_init=False):
        if(sentence_init != False):
            self._sentence_content = sentence_init
        else:
            self._sentence_content
        self._expanded_paragraph = paragraph()

    def assign_expanded_paragraph(self,paragraph_in):
        self._expanded_paragraph = paragraph_in

class paragraph():
    def __init__(self,sentences=False):
        if(sentences != False):
            self._sentences = sentences
            self._expanded_paragraphs = [paragraph()] * len(sentences)
        else:
            self._sentences = []
            self._expanded_paragraphs = []

    def add_sentence(self,sentence):
        self._sentences.append(sentence)
        self._expanded_paragraphs.append(paragraph())

    def add_sentences(self,sentence_list):
        self._sentences = self._sentences + sentence_list
        self._expanded_paragraphs = self._expanded_paragraphs + [paragraph()] * len(sentence_list)

    def assign_expanded_paragraph(self,paragraph_in,sentence_index):
        self._expanded_paragraphs[sentence_index] = paragraph_in

    def yield_sentences(self):
        for i,sentence in enumerate(zip(self._sentences,self._expanded_paragraphs),start=0):
            print str(i)+'. '+sentence[0]+' :-> '+str(len(sentence[1]._sentences))

class page():
    def __init__(self,paragraphs=False):
        if(paragraphs != False):
            self._paragraphs = paragraphs
        else:
            self._paragraphs = []

    def add_paragraph(self,paragraph):
        self._paragraphs.append(paragraph)

    def add_paragraphs(self,paragraphs):
        self._paragraphs = self._paragraphs + paragraphs

class multi_page():
    def __init__(self,pages=False):
        if(pages != False):
            self._pages = pages
        else:
            self._pages = []

    def add_page(self,page):
        self._pages.append(page)

    def add_pages(self,pages):
        self._pages = self._pages + pages

class snowflake():
    def __init__(self):
        self._one_sentence_summary = ''
        self._one_paragraph_summary = paragraph()
        self._one_page_summary = page()
        self._multi_page_summary = multi_page()
        self._list_of_scenes = []
        self._list_of_characters = []
        self._task_order = ['Write a one-sentence summary for your story.',
                            'Write a one-paragraph summary for your story emcompassing the story setup, major disasters, and ending.',
                            'For each major character, write a character sheet.',
                            'Expand each sentence of your summary into a full paragraph; each paragraph but the last must end in disaster.',
                            "Write a more detailed character sheet for each major character an a less detailed one for seconary characters; this should be the story from this character's point of view.",
                            "Expand your one-page synopsis into a four-page synposis - each paragraph becomes a full page.",
                            "Complete full character charts for all characters.",
                            "Make a list of all the scenes that will need to make up your synopsis.",
                            "Write each scene."]
        self._task_status = ['Not Started']*len(self._task_order)

    def prompt(self):
        for task in zip(self._task_order,self._task_status):
            print task
            if(task[1] == 'Not Started'):
                requested = raw_input(task[0]+'\n --> ')
                current_task = task
                break
        current_index = self._task_order.index(current_task[0])
        self._task_status[current_index] = 'Active'
        if(current_index == 0):
            self._one_sentence_summary = requested

class character_tree():
    def __init__(self):
        """
        
        """
        self._feet = paragraph()

    def define_part(self,part_key):
        """
        A) Feet - What does the person look like? What are the facts of their family history? Where have they lived? Where did they go to school? Were they poor? Do they have tattoos? Who is their best friend? What is their occupation? There are a million pertinent questions. The feet are essentially all wholly palpable details to draw on, like physical details and facts.   

        B) Groin - What does the person want? How does their sexuality manifest itself? What about them is base and puerile? Greed? Approval? Esteem? Gluttony? You can basically run toward their approach to the seven deadly sins here. The groin covers all the things about the character that are born out of impulse and desire.   

        C) Heart - What does the person need? What will make them a more functional person? Do they need to find self-confidence? Do they need to provide for their family? Do they

        D) Throat - How does the person sound? Not just the literal voice, but how does the person project themselves? How do they try to come off to other people? How do they actually come off to other people? What is their "surface vibe" as they say?  The throat is basically their posture, attempt at presentation, and affectation.   

        E) Left cheek - What is their intelligence? How does it manifest itself? What is their practicality? How do they solve problems? Basically, the left cheek is their methodology, exposing the "left-brained" abilities.   

        F) Right cheek - What is their idealistic / artistic capacity? What is their conscience and morality? What is their un-practicality? What is their spirituality? The right cheek is their ethics and soul and exposes their "right-brained" abilities.   

        G) Crown - Now, this one is the most important because this is where we look at all the body parts listed and piece them together to see how they work as an actual psychology. And that’s when you know you’re creating a complex person with a conscious mind and a subconscious id.  It’s also where you can start to piece together what really matters about this character to your story. What are their defining memories? What is their pathology? The crown essentially allows you to answer the question: Who is this person?

        H) Third Eye - Epiphany - (what will s/he learn, how will s/he change?)
        """
        self._part_dict[part_key] = paragraph()

def main():

    first_paragraph = paragraph(['Sentence A','Sentence B'])
 
    first_paragraph.add_sentence('Sentence C')
    first_paragraph.assign_expanded_paragraph(paragraph(['This is the expansion of the second sentence.','Its index would be 1.','Hopefully this works.']),1)
    first_paragraph.yield_sentences()

    """
    snowfile = open('my_flake.snowflake','r')
    snowfile_write = open('my_flake.snowflake_scratch','w')

    
    #my_flake = snowflake()
    my_flake= pickle.load(snowfile)
    my_flake.prompt()
    pickle.dump(my_flake,snowfile_write)
    os.system('mv -f my_flake.snowflake my_flake.snowflake_old')
    os.system('mv -f my_flake.snowflake_scratch my_flake.snowflake')
    """

if __name__ == "__main__":
    main()


